Conways game of life,
 
Rules:
Any live cell with fewer than two live neighbours dies, as if caused by under-population.
Any live cell with two or three live neighbours lives on to the next generation.
Any live cell with more than three live neighbours dies, as if by overcrowding.
Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

In the beginning, 
Each cell will have a 50% chance of being alive, I would personally like to change it to something like 25% but that's not important atm. 

Follow the rules and go for it