package com.hotmail.main;

import javax.swing.JFrame;

public class GameFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final int GRID_SIZE;
	
	public GameFrame(final int GRID_SIZE)
	{
		this.GRID_SIZE = GRID_SIZE;
	}
	
	public final int getGridSize()
	{
		return GRID_SIZE;
	}

}
