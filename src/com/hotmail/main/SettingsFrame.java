package com.hotmail.main;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSlider;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

public class SettingsFrame extends JFrame {

	private JPanel contentPane;
	private Game game;
	private JTextField txtSeed;
	
	/**
	 * Create the frame.
	 */
	public SettingsFrame(Game game) {
		setTitle("Settings");
		this.game = game;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 324, 215);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnStop = new JButton("Stop");
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				game.stop();
			}
		});
		btnStop.setBounds(208, 142, 89, 23);
		contentPane.add(btnStop);
		
		JButton btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				game.start();
			}
		});
		btnStart.setBounds(10, 142, 89, 23);
		contentPane.add(btnStart);
		
		JButton btnNewButton = new JButton("Restart");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				game.stop();
				game.setSeed(Integer.parseInt(txtSeed.getText()));
				game.reset();
			}
		});
		btnNewButton.setBounds(109, 142, 89, 23);
		contentPane.add(btnNewButton);
		
		txtSeed = new JTextField();
		txtSeed.setText("153245");
		txtSeed.setBounds(10, 48, 188, 20);
		contentPane.add(txtSeed);
		txtSeed.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Seed value (Reproduce same results)");
		lblNewLabel.setBounds(10, 23, 287, 14);
		contentPane.add(lblNewLabel);
		
		JSlider sldGamespeed = new JSlider();
		sldGamespeed.setValue(5);
		sldGamespeed.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				
				JSlider source = (JSlider)e.getSource();
				if(source.getValueIsAdjusting())
				{
					game.setSpeed(sldGamespeed.getValue());
				}
			}
		});
		sldGamespeed.setPaintTicks(true);
		sldGamespeed.setMajorTickSpacing(1);
		sldGamespeed.setMinimum(1);
		sldGamespeed.setMaximum(10);
		sldGamespeed.setBounds(10, 105, 287, 26);
		contentPane.add(sldGamespeed);
		
		JLabel lblSetGameSpeed = new JLabel("Set game speed:");
		lblSetGameSpeed.setBounds(10, 79, 152, 14);
		contentPane.add(lblSetGameSpeed);
		
		this.setLocationRelativeTo(this);
	}
}
