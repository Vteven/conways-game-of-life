package com.hotmail.main;

import java.util.TimerTask;

import javax.swing.JPanel;

public class TickTask extends TimerTask {

	private Game m_game;
	
	public TickTask(Game game)
	{
		m_game = game;
	}
	
	@Override
	public void run() {
		m_game.tick();
	}
	
}
