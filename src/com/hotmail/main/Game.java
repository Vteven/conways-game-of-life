package com.hotmail.main;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.Arrays;
import java.util.Random;
import java.util.Timer;

import javax.swing.JTextField;


public class Game {
	
	private JTextField[][] m_gridData;
	//True is alive
	private boolean[][] m_gridDataAsBool;
	private final float FIELD_PTS = 32f;
	private boolean m_isPlaying = false;
	//the seed will provide a constant you can change it to provide different patterns
	private int SEED = 153245;
	//percentage of the seed value to compare with, values 1 - 10 represent 10% - 100%
	private final int PERCENTAGE = 9;
	private GameFrame frame;
	//Speed 1, 2 or 3
	private int m_speed = 5;
	private Timer timer;
	private TickTask tickTimer;
	
	public Game(GameFrame frame)
	{
		//Start the tick timer
		timer = new Timer();
		tickTimer = new TickTask(this);
		timer.schedule(tickTimer, 1000, 50);
		
		this.frame = frame;
		initializeGame();
	}
	
	private void initializeGame()
	{
		//Initialize the grid arrays
		Random gen = new Random(SEED);
		
		clearData();
		frame.setLayout(new GridLayout(frame.getGridSize(), frame.getGridSize(), 0, 0));
		//Loop over every row and column adding a text field
		for(int row=0;row<m_gridData.length;row++)
		{
			for(int col=0;col<m_gridData.length;col++)
			{
				//m_gridDataAsBool initialised with all values as false, 
				//then the createCell method will change the live cells to true in the bool version
				m_gridData[row][col] = createCell(row, col);
				frame.add(m_gridData[row][col]);
				int chance = gen.nextInt((10) + 1);
				boolean willSpawn = chance > PERCENTAGE;
				if(willSpawn)
				{
					resurectCell(row, col);
				}				
			}
		}
		frame.setVisible(true);
				
	}
	
	private void clearData()
	{
		m_gridData = new JTextField[frame.getGridSize()][frame.getGridSize()];
		m_gridDataAsBool = new boolean[frame.getGridSize()][frame.getGridSize()];
	}
	
	public void setSeed(int seed)
	{
		this.SEED = seed;
	}
	
	//Speed, between 1 and 10 being 100 millisecods and 1 second
	public void setSpeed(int speed)
	{
		m_speed = speed;
	}
	
	public void reset()
	{
		stop();
		for(int row=0;row<m_gridData.length;row++)
		{
			for(int col=0;col<m_gridData.length;col++)
			{
				frame.remove(m_gridData[row][col]);
			}
		}
		initializeGame();
		start();
	}
	
	private JTextField createCell(int row, int col)
	{
		//Random number to create the initial pattern
		
		JTextField field = new JTextField(2);
		field.setHorizontalAlignment(JTextField.CENTER);
		field.setFont(field.getFont().deriveFont(Font.BOLD, FIELD_PTS));
		field.setBackground(Color.BLUE);
		field.setBorder(javax.swing.BorderFactory.createLineBorder(Color.GRAY));
		
		return field;
	}
	
	public void start()
	{
		m_isPlaying = true;
	}
	
	public void stop()
	{
		m_isPlaying = false;
	}
	
	public boolean isPlaying()
	{
		return m_isPlaying;
	}
	
	public int countNeighbours(int row, int col)
	{
		
        int total = 0;
        int radius = 1;
        /**
         * Ints are cell numbers in a square starting from top left
         * E.G 
         * -## - = start here
         * #*# * = cell
         * ### # = rest of cells
         */
        for(int x = row - radius; x <= row + radius; x++)
        {
        	for(int y = col - radius; y <= col + radius; y++)
        	{
        		
        		if(isCellExisting(x, y)) 
        		{
        			boolean isSelf = x == row && y == col;
        			if(!isSelf && isCellAlive(x, y)) {
        				total++;
        			}
        		}
        		
        	}
        }
       
        return total;
	}
	
	public boolean isCellExisting(int x, int y)
	{
		if(x < 0 || x >= m_gridData.length) return false;
		if(y < 0 || y >= m_gridData[x].length) return false;
		return true;
	}
	
	public boolean isCellAlive(int x, int y)
	{
		return m_gridDataAsBool[x][y];
	}
	
	private void killCell(int x, int y)
	{
		m_gridDataAsBool[x][y] = false;
		m_gridData[x][y].setBackground(Color.BLUE);
	}
	
	private void resurectCell(int x, int y)
	{
		m_gridDataAsBool[x][y] = true;
		m_gridData[x][y].setBackground(Color.DARK_GRAY);		
	}
	
	int current = 1;
	public void tick()
	{

		if(!m_isPlaying) return;
		if(current <= m_speed) 
		{
			current ++;
			return;
		} else
		{
			current = 1;
		}

		//Loop over the grid data every tick
		for(int row=0;row<m_gridData.length;row++)
		{
			for(int col=0;col<m_gridData.length;col++)
			{

				//Is the cell alive or dead
/*				boolean isCellDead = cell.getBackground() == Color.BLUE ? true : false;*/

				//Check the neighboring cells to help determine its future state				
				int numOfNeighbours = countNeighbours(row, col);

				//Based on the number of neighbours for each cell, a corresponding state
				//in the bool version of the grid will be changed to T/F
				
				//Rules that are stated only if the current cell is alive
				if(isCellAlive(row, col))
				{
					if(numOfNeighbours < 2)
					{
						killCell(row, col);
					} else if(numOfNeighbours > 3)
					{
						killCell(row, col);
					}
				} else
				{
					if(numOfNeighbours == 3)
					{
						resurectCell(row, col);
					}
				}
			}
		}
	}
	
}
