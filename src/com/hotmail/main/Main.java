package com.hotmail.main;

import java.awt.Button;
import java.awt.Color;
import java.util.Timer;

import javax.swing.JFrame;

public class Main {

	/**
	 * How many grid locations vertically and horizontally
	 * E.G 16 - total number of cells = 16 * 16
	 * 
	 */
	private final static int GRID_SIZE = 32;
	
	public static void main(String[] args){
		//Creating JFrame
		GameFrame panel = new GameFrame(GRID_SIZE);
		panel.setSize(600, 600);
		
		//Initialize the game
		Game game = new Game(panel);
		
		SettingsFrame settings = new SettingsFrame(game);
		settings.setVisible(true);
		
	}

}
